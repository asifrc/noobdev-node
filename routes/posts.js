// Bismillah

/**
* This module contains functions for managing and routing posts
*/
var fs = require('fs'),
	jade = require('jade'),
	marked = require('marked');

var jsdoc = require('./jsondoc');

var PPath = "./posts"; //Default Path

var docs = jsdoc.newDoc();

var path = function(fpath) {
	PPath = (typeof fpath === "string") ? fpath : PPath;
	docs.load(PPath+"/posts.json", function(err) {
		importDocs();
	});
};
module.exports.loadPath = path;

// Creates a slug/anchor-friendly name for a given text
var slugify = function(name) {
	var slug = name.toLowerCase().replace(/[^A-Za-z0-9]/g, "-").replace(/-[-]+/g, "-");
	slug = slug.replace(/-$/, "");
	return slug;
};

conlog = 0;
var de = function(txt) {
	txt = (typeof txt !== "undefined") ? " - "+txt : "";
	console.log("Debug Log: "+(conlog++),txt);
};
de('Debugger Initialized..'); //DEBUG

// Sort Posts
var sortBy = function(arr,key) {
	return arr.key.sort(function(a,b) {
		return a[key] > b[key];
	});
};


// Array.Remove 
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

// Filters articles by options and returns



// Updates posts.json with file system
var importDocs = function(cb) {
	// Valid Extensions
	var exts = [
		"md",	// MarkDown
		"jade",	// Jade
		"html"	// Plain Old HTML
	];
	// Excerpt modifieer (e.g. "x" for when file.x.md represents file.md)
	var excerpt = "x";
	var ext = new RegExp("^((?!\\."+excerpt+").)*((\\."+ exts.join(")|(\\.") + "))$");
	var nExt = new RegExp("\\.((" + exts.join(")|(")+"))$");
	
	// Merges fresh file list with existing posts in post manifest (PPath/posts.json)
	var merge = function(err, newFiles) {
		if (err)
		{
			// Call callback with error
			cb(err);
		}
		else
		{
			docs.data.posts = (typeof docs.data.posts === "object") ? docs.data.posts : [];
			
			var checkUnique = function(subject, comparison, actOn) {
				// Loop through each file in the subject and check if it exists in the comparison
				for (var i=0; i<subject.length; i++)
				{
					var unique = comparison.reduce( function(notMatched,post) {
								return notMatched && (post.name!=subject[i].name); 
							}, true);
					if (unique)
					{
						actOn(i);
					}
				}
			}
			
			// Add new files to manifest
			checkUnique(newFiles, docs.data.posts, function(index) {
				// Add new post to posts manifest
				console.log("Adding new post '" + newFiles[index].path + "' ...");
				docs.data.posts.push( newFiles[index] );
			});
			
			var stale = [];
			// Remove any files that no longer exist
			checkUnique(docs.data.posts, newFiles, function(index) {
				console.log("Removing non-existent post '" + newFiles[index].path + "' ...");
				stale.push(index);
			});
			for (var i=(stale.length-1); i >= 0; i--)
			{
				// Remove post from manifest
				console.log(docs.data.posts, stale[i]);
				console.log("Removing old post '"+docs.data.posts[stale[i]].path+"' ...");
				docs.data.posts.remove(stale[i]);
			}
			
			// Save changes made to posts manifest, and pass the original callback
			docs.save(cb);
		}
	};
	
	// Reads all directories in PPath, places their files in filepath and calls merge()
	fs.readdir(PPath, function(err, cats) {
		var fileList = [];
		// Except for posts.json, try to open each file as a dir and grab files
		var list = function(error,x,cback) {
			// If we haven't gone through all the files in PPath
			if (!error && x<cats.length)
			{
				if (!cats[x].match(/\./))
				{
					fs.readdir(PPath+"/"+cats[x], function(err, posts) { 
						if (err)
						{
							// Call merge() with error to pass up to cb
							merge(err, fileList);
						}
						else
						{
							// Go through each file in the category folder
							for (var i=0; i<posts.length; i++)
							{
								// If the file has a valid extension
								if (posts[i].match(ext))
								{
									var fullPath = PPath+"/"+cats[x]+"/"+posts[i];
									
									// Push filename and directory onto files list
									fileList.push({
										'name': slugify(posts[i].replace(nExt, "")),
										'title': slugify(posts[i].replace(nExt, "")),
										'type': posts[i].match(nExt)[0].substr(1),
										'path': fullPath,
										'excerpt': (fullPath).replace(nExt, ".x"+fullPath.match(nExt)[0]),
										'categories': [ cats[x] ],
										'dir': cats[x],
										'status': "draft",
										'date': new Date()
									});
								}
							}
							// Run list() again with x incremented
							cback(err,x+1, cback)
						}
					});
				}
				else
				{
					cback(error, x+1, cback);
				}
			}
			else
			{
				// Call merge() to merge newly aggregated files with old files
				merge(error, fileList);
			}
		};
		list(null, 0, list);
	
	});
}



module.exports.dev = function(req, res) {
	//docs.data.posts[0].categories = ['Getting Started', 'Something or Something'];
	console.log(docs);
	if (typeof docs.data.posts === "object")
	{
		res.send(JSON.stringify(docs.find("posts", { categories: "wp-old" })));
	}
	else
	{
		res.send("Posts not loaded");
	}
};

var pageNotFound = function(req, res) {
	res.render( 'article404');
};

var showArticle = function(req, res) {
	var articles = docs.find('posts',{ name: req.params.slug.toLowerCase() });
	if (articles.length>0)
	{
		var article = articles[0];
		
		// Format article date
		article.post_date = new Date(article.date);
		var date = article.post_date.getMonth() + "/";
		date += article.post_date.getDate() + "/";
		date += article.post_date.getFullYear();
		article.post_date = new Date(article.date);
		
		// Determine how to render content into HTML
		var prerender = function(con) { return con; };
		switch(article.type)
		{
			case "md":
			{
				prerender =  marked;
				break;
			}
			case "jade":
			{
				prerender = function(con) { return jade.compile(con)(); };
			}
		}
		
		//Load File
		fs.readFile(article.path, function(err, content) {
			if (!err)
			{
				// Render contents into HTML
				article.content = prerender(content.toString());
				// Generate Table of Contents and inject anchors into Content
				var ToC = autoTOC(article.content);
				// Replace crayon with highlighter [BACKWARDS COMPATIBILITY WITH WORDPRESS]
				article.content = ToC.content.replace(/<pre(?![\s]*class=".*crayon:false.*")/g, '<pre class="brush: cpp"');
				article.post_title = article.title;
				
				// Send to article.jade to be rendered to output
				res.render( 'article', { post: article, toc: ToC.toc} );
			}
			else
			{
				console.log(err); //DEBUG
				pageNotFound(req, res);
			}
		});
	}
	else
	{
		pageNotFound(req,res);
	}
};

module.exports.article = function(req, res) {
	if (typeof req.params.slug === "string")
	{
		console.log({ name: req.params.slug });
		showArticle(req, res);
	}
	else
	{
		pageNotFound(req, res);
	}
};

/**
	Table of Contents Generator
*/
// Creates an anchor-friendly name for a phrase
var anchorName = slugify;

//Find a unique anchorname
var uniqueAnchor = function(anchors, name)
{
	var uName = name;
	if (anchors.indexOf(uName))
	{
		var base = 1;
		while (anchors.indexOf(uName)!=-1)
		{
			uName = name + "[" + (base++) + "]";
		}
	}
	
	return uName;
};

// Extracts a table of contents from the article's headers and adds linkable
// anchors to the body
var autoTOC = function(text) {
console.log(typeof text);
	// Go through text and parse out headers
	var tx = text;
	var headers = [];
	var anchors = [];
	var reTagOpen = /<h[1-7]/i;
	var reTagClose = /<\/h[1-7]>/i;
	for (i=0; i<tx.length; i++)
	{
		var tag = tx.substr(i,3).match(reTagOpen);
		if (tag)
		{
			var pos1 = false;
			var pos2 = false;
			for (j=i; (j<tx.length && !pos2); j++)
			{
				pos1 = (tx.charAt(j)==">") ? j : pos1;
				if (pos1 && tx.substr(j,5).match(reTagClose))
				{
					pos2 = j;
					
					//Find a valid anchor
					anchor = uniqueAnchor(anchors, anchorName(tx.substring(pos1+1, pos2)));
					
					// Set up object 
					var item = {
						'tag': tag[0].substr(1),
						'text': tx.substring(pos1+1, pos2),
						'anchor': anchor,
						'level': parseInt(tag[0].substr(2))
					};
					
					// Push header info into headers array
					headers.push(item);
					anchors.push(anchor);
					
					// Insert id
					tx = tx.substr(0, pos1) + ' class="anchor" id="' + anchor + '"' + tx.substr(pos1);
				}
			}
		}
	}
	
	// return a nested TOC from the headers
	var tabs = 0;
	var tab = function(n) {
		txt = "";
		for (var i=0; i<n; i++)
		{
			txt += "\t";
		}
		return txt;
	};
	
	var levels = [0];
	var toc = "ul.nav.bs-sidenav\n";
	
	toc += "\tli.tocTitle\n";
	toc += "\t\tstrong Table of Contents\n\n";
	
	for (var i=0; i<headers.length; i++)
	{
		var index = levels[levels.length-1];
		var h = headers[i];
		if (h.level>index)
		{
			index = h.level;
			if (i>0)
			{
				levels.push(8);
				toc += tab(levels.length-1) + "ul.nav\n";
			}
			levels.push(index)
		}
		while (h.level<index & levels.length>1)
		{
			levels.pop();
			index = levels[levels.length-1];
		}
		toc += tab(levels.length-1) + "li\n";
		toc += tab(levels.length) + 'a(href="#' + h.anchor + '") ' + h.text + "\n";
	}
	
	toc2Html = jade.compile(toc)();
	
	ret = {
		content: tx,
		toc: toc2Html
	};
	return ret;
		
};