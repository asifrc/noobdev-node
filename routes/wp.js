// Bismillah

/**
* This file contains all interactions with the wordpress database
* Needs to be initialized before first use.
* Should be required as follows:
*     wp = require('./routes/wp.js');
*     wp.init();
*/

var mysql = require("mysql");
var config = require('../config.js');
var pool = mysql.createPool(config.database);

//Require jade for templating functions
var jade = require('jade');


// Grabs a connection, executes a query and then an optional callback
var query =  function(sql, callback) {
	pool.getConnection(function(error, db) {
		if (error)
		{
			console.log("Unable to connect",error);
			res.send("Unable to connect: "+error);
			db.release();
		}
		else
		{
			db.query(sql, function(err, result) {
				if (err)
				{
					console.log("Query failed",err);
					res.send("Query Failed "+err);
				}
				else
				{
					if (typeof callback === "function")
					{
						callback(result);
					}
				}
				db.release();
			});
		}
	});
};

// A list of all articles
module.exports.all = function(req, res) {
	var sql = "SELECT `post_name`,`post_date`, `post_title`";
	sql += " FROM `wp_posts` WHERE `post_status`=\"publish\" ORDER BY `post_date` DESC";
	query(sql, function(rows) {
		rows.map( function(row) {
			d = new Date(row.post_date);
			date = d.getMonth() + "/";
			date += d.getDate() + "/";
			date += d.getFullYear();
			row.date = date;
		});
		res.render( 'all', { posts: rows } );
	});
};

//Steps each header tag one level down
var hdown = function(text) {
	var sdown = function(txt, level) {
		var reOpen = new RegExp("<h"+level, "gi");
		var reClose = new RegExp("</h"+level, "gi");
		var newLevel = level + 1;
		var ret =  txt.replace(reOpen, "<h"+newLevel);
		return ret.replace(reClose, "</h"+newLevel);
	};
	
	var tx = text;
	for (var i=6; i>0; i--)
	{
		tt = sdown(tx, i);
		// DEV: For some reason, tx reassigning itself causes a bug..
		tx = tt;
	}
	return tx;
};

// Creates an anchor-friendly name for a phrase
var anchorName = function(name) {
	anchor = name.toLowerCase().replace(/[^A-Za-z0-9]/g, "-").replace(/-[-]+/g, "-");
	anchor = anchor.replace(/-$/, "");
	return anchor;
};

//Find a unique anchorname
var uniqueAnchor = function(anchors, name)
{
	var uName = name;
	if (anchors.indexOf(uName))
	{
		var base = 1;
		while (anchors.indexOf(uName)!=-1)
		{
			uName = name + "[" + (base++) + "]";
		}
	}
	
	return uName;
};

// Extracts a table of contents from the article's headers and adds linkable
// anchors to the body
var autoTOC = function(text) {
	// Go through text and parse out headers
	var tx = text;
	var headers = [];
	var anchors = [];
	var reTagOpen = /<h[1-7]/i;
	var reTagClose = /<\/h[1-7]>/i;
	for (i=0; i<tx.length; i++)
	{
		var tag = tx.substr(i,3).match(reTagOpen);
		if (tag)
		{
			var pos1 = false;
			var pos2 = false;
			for (j=i; (j<tx.length && !pos2); j++)
			{
				pos1 = (tx.charAt(j)==">") ? j : pos1;
				if (pos1 && tx.substr(j,5).match(reTagClose))
				{
					pos2 = j;
					
					//Find a valid anchor
					anchor = uniqueAnchor(anchors, anchorName(tx.substring(pos1+1, pos2)));
					
					// Set up object 
					var item = {
						'tag': tag[0].substr(1),
						'text': tx.substring(pos1+1, pos2),
						'anchor': anchor,
						'level': parseInt(tag[0].substr(2))
					};
					
					// Push header info into headers array
					headers.push(item);
					anchors.push(anchor);
					
					// Insert id
					tx = tx.substr(0, pos1) + ' class="anchor" id="' + anchor + '"' + tx.substr(pos1);
				}
			}
		}
	}
	
	// return a nested TOC from the headers
	var tabs = 0;
	var tab = function(n) {
		txt = "";
		for (var i=0; i<n; i++)
		{
			txt += "\t";
		}
		return txt;
	};
	
	var levels = [0];
	var toc = "ul.nav.bs-sidenav\n";
	
	toc += "\tli.tocTitle\n";
	toc += "\t\tstrong Table of Contents\n\n";
	
	for (var i=0; i<headers.length; i++)
	{
		var index = levels[levels.length-1];
		var h = headers[i];
		if (h.level>index)
		{
			index = h.level;
			if (i>0)
			{
				levels.push(8);
				toc += tab(levels.length-1) + "ul.nav\n";
			}
			levels.push(index)
		}
		while (h.level<index & levels.length>1)
		{
			levels.pop();
			index = levels[levels.length-1];
		}
		toc += tab(levels.length-1) + "li\n";
		toc += tab(levels.length) + 'a(href="#' + h.anchor + '") ' + h.text + "\n";
	}
	
	toc2Html = jade.compile(toc)();
	
	ret = {
		content: tx,
		toc: toc2Html
	};
	return ret;
		
}


// Displays a single article
module.exports.article = function(req, res) {
	var slug = req.params.slug;
	console.log(req.params);
	var sql = 'SELECT * FROM `wp_posts` WHERE `post_name`="'+slug+'"';
	query(sql, function(rows) {
		if (rows.length==1)
		{
			rows.map( function(row) {
				d = new Date(row.post_date);
				date = d.getMonth() + "/";
				date += d.getDate() + "/";
				date += d.getFullYear();
				row.date = date;
				frow = autoTOC( row.post_content );
				row.content = hdown( frow.content ).replace(/<pre(?![\s]*class=".*crayon:false.*")/g, '<pre class="brush: cpp"');
				row.toc = frow.toc;
			});
			res.render( 'article', { post: rows[0], toc: rows[0].toc} );
		}
		else
		{
			res.render( 'article404');
		}
	});
};


// The Directory..
module.exports.dir = function(req, res) {
	var sql = "SELECT `post_name`,`post_date`, `post_title`, `post_content`";
	sql += " FROM `wp_posts` WHERE `post_status`=\"publish\" ORDER BY `post_date` DESC";
	query(sql, function(rows) {
		rows.map( function(row) {
			d = new Date(row.post_date);
			date = d.getMonth() + "/";
			date += d.getDate() + "/";
			date += d.getFullYear();
			row.date = date;
		});
		
		res.render( 'all.jade', { posts: rows } );
	});
};