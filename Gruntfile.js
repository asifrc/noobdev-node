// Bismillah

// First Gruntfile
exec = require('child_process').exec
module.exports = function(grunt) {
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		simplemocha: {
			options: {
				reporter: 'spec'
			},
			all: { src: ['test/*.js'] }
		}
			
	});
	
	grunt.loadNpmTasks('grunt-simple-mocha');
	
	// DEFAULT TASK
	grunt.registerTask('default', ['simplemocha']);
	
	
};