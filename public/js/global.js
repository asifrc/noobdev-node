// Bismillah

/**
* Begin Bootstrap Scrap
*/
$(function(){

    var $window = $(window)
    var $body   = $(document.body)

    var navHeight = $('.bs-sidebar').offset().top + 10

    $body.scrollspy({
      target: '.bs-sidebar',
      offset: navHeight
    })

    $window.on('load', function () {
      $body.scrollspy('refresh')
    })

    $('.bs-docs-container [href=#]').click(function (e) {
      e.preventDefault()
    })

    // back to top
    setTimeout(function () {
      var $sideBar = $('.bs-sidebar')

      $sideBar.affix({
        offset: {
          top: function () {
            var offsetTop      = $sideBar.offset().top + 50;
            var sideBarMargin  = parseInt($sideBar.children(0).css('margin-top'), 10)
            var navOuterHeight = $('.navbar-fixed-top').height();
			
			console.log(offsetTop,sideBarMargin, navOuterHeight);

            return (this.top = offsetTop - navOuterHeight - sideBarMargin);
          }
        }
      })
    }, 1)
	 /**
	 * End Bootstrap crap
	 */
 });
 
//Syntax Highlighter
SyntaxHighlighter.all()
