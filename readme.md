Bismillah

#N00BD3V Website

This is the node.js website that synchronizes posts with noobdev.asifrc.com

It implements Twitter Bootstrap as a frontend framework, allowing rapid
development but with more flexibility than the WordPress Framework.

By syncing with the WordPress database, we get the best of both worlds.