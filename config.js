// Bismillah

/**
* Config file
* -----------
* Contains any configurable info
*/

module.exports = {
	// WordPress Mysql Database Settings
	database: {
		host: "hosturl",
		database: "databasename",
		user: "username",
		password: "password!"
	}
};
		