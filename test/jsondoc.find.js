// Bismillah

/**
* Test Suite for jsondoc.find()
*/

assert = require('assert');
should = require('should');

jsdoc = require('../routes/jsondoc');
var otype =  function(param) { return Object.prototype.toString.call(param); };

describe("Bismillah", function() {
	it("Mocha Loads!", function() {
		assert.ok;
	});
});

/**
	Mock Values
*/

var item = [
	{
		name: 'item0',
		type: 'html',
		categories: ['cat1'],
	},
	{
		name: 'item1',
		type: 'html',
		categories: ['cat1', 'cat2'],
	},
	{
		name: 'item2',
		type: 'md',
		categories: ['cat1', 'cat2'],
	},
	{
		name: 'item3',
		type: 'md',
		categories: [],
	}
];

var match = {
	name_item1: [
		item[1]
	],
	type_md: [
		item[2],
		item[3]
	],
	category_cat0: [],
	category_cat1: [
		item[0],
		item[1],
		item[2]
	],
	category_cat2: [
		item[1],
		item[2]
	]
};

/**
	Begin Tests
*/

describe("jsondoc", function() {
	describe("Load posts.json", function() {
		it("should load posts.json", function(done) {
			var tdoc = jsdoc.newDoc('./posts/posts.json');
			assert.equal(typeof tdoc.data, "object");
			done();
		});
	});
	var doc;
	
	beforeEach(function() {
		doc = jsdoc.newDoc();
		doc.data.posts = item;
	});
	
	describe(".find()", function() {
	
		it("matches a single item with a unique value", function() {
			doc.find('posts', { name: 'item1'}).should.eql(match.name_item1);
		});
		it("matches multiple items with nonunique values", function() {
			doc.find('posts', { type: 'md'}).should.eql(match.type_md);
		});
		it("matches a single item with multiple fields with one result", function() {
			doc.find('posts', { name: 'item1', type: 'html'}).should.eql(match.name_item1);
		});
		it("matches a single item with multiple fields with one result - reversed", function() {
			doc.find('posts', { type: 'html', name: 'item1'}).should.eql(match.name_item1);
		});
		it("returns [] when element not found in any arrays values", function() {
			doc.find('posts', { categories: ['cat0'] }).should.eql([]);
		});
		it("matches multiple items with a single common array elements", function() {
			doc.find('posts', { categories: ['cat1'] }).should.eql(match.category_cat1);
		});
		it("matches multiple items with multiple common array elements", function() {
			doc.find('posts', { categories: ['cat1','cat2'] }).should.eql(match.category_cat2);
		});
		
		
		// Empty results
		describe("returns [] when", function() {
			it("returns [] when no parameters provided", function() {
				var result = doc.find();
				assert.equal(otype(result), "[object Array]");
				assert.equal(result.length, 0);
			});
			it("returns [] when one non-string parameter provided", function() {
				var result = doc.find({});
				assert.equal(otype(result), "[object Array]");
				assert.equal(result.length, 0);
			});
			it("returns [] when passed a nonexistent category", function() {
				
			});
			it("returns [] when passed a nonexistent key", function() {
				
			});
			it("returns [] when passed a valid key with non-existent value", function() {
				
			});
		});	
	});
});