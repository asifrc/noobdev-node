// Bismillah

/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// Load WP module
var wp = require('./routes/wp');
var posts = require('./routes/posts');
posts.loadPath('./posts');

/**
* Routes
*/

// Homepage
app.get('/', routes.index);

// List All Posts
app.get('/all', wp.all);
app.get('/articles', wp.dir); // Temporary
app.get('/articles/:slug', posts.article);
/**
* DEV
*/
app.get('/dev/jsdoc', posts.dev);
app.get('/dev/articles/:slug', posts.article);

app.get('/dev/all', function(req, res) {
	var sql = "SELECT * FROM `wp_posts` WHERE `post_status`=\"publish\" ORDER BY `post_date` DESC";
	query(sql, function(rows) {
		res.send( JSON.stringify(rows) );
	});
});


http.createServer(app).listen(app.get('port'), function(){
  console.log('Bismillah - Express server listening on port ' + app.get('port'));
});
